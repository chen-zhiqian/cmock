package utils;

// 平台内置提示信息
public class ResponseContent {

    // 异步回调的提示信息
    public static final String PROMPTFORCALLBACK = "已收到请求，待服务器处理完毕会将响应返回到指定地址！！！";

    // mock文件不存在
    public static final String FILENOTEXIST = "该mock文件不存在，请重新指定！";

    // 该规则无效时
    public static final String INVALIDRULE = "规则有误或无法判断该请求，请查看文档修改规则的内容或格式！！！";

    // 输入参数的个数错误
    public static final String PARAMETERSNUMWRONG = "{\\\"retCode\\\":\\\"401\\\",\\\"retMsg\\\":\\\"请求参数个数不符合，请查阅规则！！！\\\"}";

    // 输入参数的KEY错误
    public static final String PARAMETERSKEYWRONG = "{\\\"retCode\\\":\\\"402\\\",\\\"retMsg\\\":\\\"请求参数的KEY有误，请核实！\\\"}";

    // 异步回调的地址有误
    public static final String ADDRESSWRONG = "{\\\"retCode\\\":\\\"403\\\",\\\"retMsg\\\":\\\"异步回调的地址有误或服务未开启，请核实！\\\"}";

}
