package com.MyHomework.cmock.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import utils.ResponseContent;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;


// 子线程工作的地方
@Service
public class ChildThreadDo implements Runnable {

    // 父线程与子线程共享指定类型参数
    public static InheritableThreadLocal<Map<String, String[]>> inheritableThreadLocal = new InheritableThreadLocal<>();
    // 手动时延
    public static int DELAY = 3000;

    public void dostm(Map<String, String[]> requestMap) {

        // 把请求参数装入inheritableThreadLocal从而使子线程共享该Map
        inheritableThreadLocal.set(requestMap);
        Thread t = new Thread(new ChildThreadDo());

        t.start();

    }

    @SneakyThrows
    @Override
    public void run() {

        // 取出共享的参数
        Map<String, String[]> requestMap = inheritableThreadLocal.get();
        Thread.sleep(DELAY);

        String notifyUrl = requestMap.get("notifyUrl")[0];
        requestMap.remove("notifyUrl");

        PostProcess postProcess = new PostProcess();
        String response = postProcess.process(requestMap);


        MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<>();
        paramMap.add("Response", response);

        try {
            postInvocation(notifyUrl, paramMap);
        } catch (Exception e) {
            System.out.println(ResponseContent.ADDRESSWRONG);
        }

    }


    // 利用RestTemplate发送post请求
    public static String postInvocation(String url, MultiValueMap<String, Object> param) {
        RestTemplate restTemplate = new RestTemplate();
        //设置Http Header
        HttpHeaders headers = new HttpHeaders();
        //设置请求媒体数据类型
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        //设置返回媒体数据类型
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(param, headers);
        return restTemplate.postForObject(url, httpEntity, String.class);
    }


}
