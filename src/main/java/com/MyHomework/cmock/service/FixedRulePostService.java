package com.MyHomework.cmock.service;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface FixedRulePostService {

    CompletableFuture<String> DoRule(Map<String, String[]> parameterMap) throws InterruptedException;

}
