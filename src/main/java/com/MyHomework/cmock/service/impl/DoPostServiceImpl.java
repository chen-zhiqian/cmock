package com.MyHomework.cmock.service.impl;

import com.MyHomework.cmock.service.DoPostService;

import org.springframework.stereotype.Service;
import utils.ResponseContent;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.Map;


@Service
public class DoPostServiceImpl implements DoPostService {


    @Override
    public String DoPost(HttpServletRequest request) throws InterruptedException, IOException {


        // 判断是直接返回还是异步回调
        Map<String, String[]> requestMap = request.getParameterMap();
        String callMode = requestMap.get("callMode")[0];

        // 两种模式
        if (callMode.equals("directProcessing")) {   // 直接处理

            PostProcess postProcess = new PostProcess();
            return postProcess.process(requestMap);

        } else {   // 异步回调

            // 开启子线程
            ChildThreadPost childThreadPost = new ChildThreadPost();
            childThreadPost.dostm(request.getParameterMap());

        }

        // 异步回调时第一个响应
        return ResponseContent.PROMPTFORCALLBACK;
    }

}


