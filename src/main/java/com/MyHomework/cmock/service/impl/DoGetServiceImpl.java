package com.MyHomework.cmock.service.impl;

import com.MyHomework.cmock.service.*;

import org.springframework.stereotype.Service;
import utils.ResponseContent;

import java.io.*;
import java.util.HashMap;


@Service
public class DoGetServiceImpl implements DoGetService{


    @Override
    public String DoGet(HashMap<String, String[]> requestMap) throws IOException {


        // 判断是直接返回还是异步回调
        String callMode = requestMap.get("callMode")[0];

        // 两种模式
        if (callMode.equals("directProcessing")) {   // 直接处理

            GetProcess getProcess = new GetProcess();
            return getProcess.process(requestMap);

        } else {   // 异步回调

            // 开启子线程
            ChildThreadGet childThreadGet = new ChildThreadGet();
            childThreadGet.dostm(requestMap);

        }

        // 异步回调时第一个响应
        return ResponseContent.PROMPTFORCALLBACK;
    }

}


