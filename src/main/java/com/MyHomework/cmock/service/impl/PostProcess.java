package com.MyHomework.cmock.service.impl;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.springframework.stereotype.Service;
import utils.ResponseContent;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.HashMap;
import java.util.Map;


// 这是之前DoPost的内容，也就是Post执行的整个过程
@Service
public class PostProcess {

    static String PATH = "./mockRule/";

    public String process(Map<String, String[]> requestMap) throws IOException {

        String response = "";   // 返回字符串

        // 判断指定的mock文件是否存在
        String mockName = jugemockName(requestMap);
        if (mockName.equals("null")) {

            return ResponseContent.FILENOTEXIST;
        }

        // 读取Json规则文件
        String jsonRule = getRule(PATH + mockName +".json");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode ruleNode = mapper.readTree(jsonRule);   // 将json字符串转换成JsonNode对象

        // 捕获异常：当规则无效时，返回内容
        try{
            // 真正对比规则的函数
            response = compareRule(ruleNode, requestMap);
        } catch (Exception e) {
            return ResponseContent.INVALIDRULE;
        }
//        response = compareRule(ruleNode, requestMap);

        return response;
    }

    // 检查指定的mock文件是否存在
    String jugemockName(Map<String, String[]> parameterMap) {
        String mockName = parameterMap.get("mockName")[0];
        File file = new File(PATH + mockName + ".json");
        if(file.exists()) {
            return mockName;
        }
        return "null";
    }

    // 从本地获取Json字符串
    private String getRule(String path) {

        String jsonStr = "";

        try {
            File jsonFile = new File(path);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");

            int chr = 0;
            StringBuffer stringBuffer = new StringBuffer();
            while ((chr = reader.read()) != -1) {
                stringBuffer.append((char)chr);
            }

            reader.close();
            jsonStr = stringBuffer.toString();
            return jsonStr;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }


    // 规则对比函数
    private String compareRule(JsonNode ruleNode, Map<String, String[]> requestMap) throws JsonProcessingException {

        String response = "响应的初始设定~";

        // 将规则中的内容取出
        ObjectMapper mapper = new ObjectMapper();
        JsonNode responseConditionNode = mapper.readTree(String.valueOf(ruleNode.findValue("responseCondition")));

        // 将请求的键值对放入MapContext中
        int index = 0;
        String requestParamTemplate = String.valueOf(ruleNode.findValue("requestParamTemplate"));
        String str = "";
        for (int i = 2; i < requestParamTemplate.length() - 2; ++i) {
            if (requestParamTemplate.charAt(i) != ' ') {
                str += requestParamTemplate.charAt(i);
            }
        }
        String rpt[] = str.split(",");
        Map rptMap = new HashMap();
        for (int i = 0; i < rpt.length; ++i) {
            String key = rpt[i].substring(0, rpt[i].indexOf(':'));
            String value = rpt[i].substring(rpt[i].indexOf(':') + 1);
            rptMap.put(key, value);
        }

        // 删除requestMap中参数之外的信息，防止影响后续规则判断
        requestMap.remove("mockName");
        requestMap.remove("callMode");

        // 检查参数个数是否符合规则要求
        if (rptMap.size() != requestMap.size()) {
            return ResponseContent.PARAMETERSNUMWRONG;
        }

        // 将参数放入MapContext
        JexlContext groundTruthContext = new MapContext();
        for (Map.Entry<String, String[]> entry : requestMap.entrySet()) {

            // 根据参数类型来放入数据
            if (rptMap.get(entry.getKey()) == null) {
                return ResponseContent.PARAMETERSKEYWRONG;
            }
            if (rptMap.get(entry.getKey()).equals("String")) {
                groundTruthContext.set(entry.getKey(), entry.getValue()[0]);
            } else {
                groundTruthContext.set(entry.getKey(), new Integer(entry.getValue()[0]));
            }

        }


//        // 检查groundTruthContext中参数个数是否正确，参数的键是否正确
//        String bufStr = jugeContext(groundTruthContext, response, ruleNode, index);
//        if (!bufStr.equals("没有对应的规则或逻辑矛盾~")) {
//            return bufStr;
//        }


        JexlEngine engine = new JexlEngine();

        for (JsonNode buf : responseConditionNode) {
            String expressionStr = String.valueOf(buf.findValue("resCondition"));

            // 对规则稍作处理
//            expressionStr = expressionStr.substring(1, expressionStr.length() - 1);
            String expStr = "";
            for (int i = 1; i <= expressionStr.length() - 2; ++i) {
                if (expressionStr.charAt(i) != '\\') {
                    expStr += expressionStr.charAt(i);
                }
            }

            // 执行语句~
            Expression expression = engine.createExpression(expStr);
            Object evaluate = expression.evaluate(groundTruthContext);
            if (evaluate.equals(true)) {
                response = String.valueOf(buf.findValue("resValue"));
            }
        }

        return response;
    }


    // 判断请求参数是否正确
    private String jugeContext(JexlContext groundTruthContext, String response, JsonNode ruleNode, int index) {

        int requestParamNum = Integer.valueOf(String.valueOf(ruleNode.findValue("requestParamNum")));
        String requestParamTemplate = String.valueOf(ruleNode.findValue("requestParamTemplate"));
        if (index != requestParamNum) {
            response = ResponseContent.PARAMETERSNUMWRONG;
            return response;
        }

        String str = "";
        for (int i = 2; i < requestParamTemplate.length() - 2; ++i) {
            if (requestParamTemplate.charAt(i) != ' ') {
                str += requestParamTemplate.charAt(i);
            }
        }

        String buf[] = str.split(",");
        for (int i = 0; i < buf.length; ++i) {
            String Param = buf[i].substring(0, buf[i].indexOf(':'));
            if (!groundTruthContext.has(Param)) {
                response = "{\\\"retCode\\\":\\\"302\\\",\\\"retMsg\\\":\\\"请求参数的KEY有误。\\\"}";
                return response;
            }
        }

        return "没有对应的规则或逻辑矛盾~";
    }

}
