package com.MyHomework.cmock.service.impl;

import com.MyHomework.cmock.service.FixedRulePostService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Service
public class FixedRulePostServiceImpl implements FixedRulePostService {

    static int DELAY = 2000;

    @Override
    @Async("threadPoolExecution")
    public CompletableFuture<String> DoRule(Map<String, String[]> parameterMap) throws InterruptedException {

        Thread.sleep(DELAY);

        String str = "";
        int num = parameterMap.size();
        if (num != 3) {
            return CompletableFuture.completedFuture("请求参数个数不匹配，请观察参数！");
        } else {
            // 分解参数
            String name = "";
            String age = "";
            String habby = "";
            for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
                if (entry.getKey().equals("name")) {
                    name = entry.getValue()[0];
                }
                if (entry.getKey().equals("age")) {
                    age = entry.getValue()[0];
                }
                if (entry.getKey().equals("habby")) {
                    habby = entry.getValue()[0];
                }
            }

            if (name.equals("") || age.equals("") || habby.equals((""))) {
                return CompletableFuture.completedFuture("缺少必要参数！");
            }
            // 规则对照
            str = doMockRule(name, age, habby);
        }
        return CompletableFuture.completedFuture(str);
    }

    // 后台直接定制mock规则。
    @Async("threadPoolExecution")
    public String doMockRule(String name, String age, String habby) {

        if (name.equals("chenzhiqian")) {
            if (!age.equals("25")) {
                return "年龄不正确！";
            }
            if (!habby.equals("听音乐")) {
                return "此人没有这个爱好。";
            }
            return "true, 陈志乾的信息完全匹配~";

        } else if (name.equals("zhengyanan")) {
            if (!age.equals("25")) {
                return "年龄不正确！";
            }
            if (!habby.equals("看电影")) {
                return "此人没有这个爱好。";
            }
            return "true, 郑亚楠的信息完全匹配~";

        } else {
            return "查无此人！";
        }

    }
}
