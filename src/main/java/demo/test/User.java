package demo.test;

/*
 *
 * 这个文件只是测验，后面要删的
 *
 * */

public class User {

    private int id;
    private String username;
    private String password;

    public User(int id, String name, String pwd) {

        this.id = id;
        this.username = name;
        this.password = pwd;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
