package demo.Jexl;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;

public class demo2 {

    public static void main(String[] args) {


        // 创建表达式引擎对象
        JexlEngine engine = new JexlEngine();
        // 创建表达式语句
        String expressionStr = "!name.equals(\"chen\") or age!=30";
        // 创建Context对象，为表达式中的未知数赋值
        JexlContext context = new MapContext();
        String str = "chen";
        context.set("name", str);           // context.set("StringUtil", new StringUtil());  // set之后可以调用StringUtil中的所有方法
        context.set("age", 30);
        // 使用表达式引擎创建表达式对象
        Expression expression = engine.createExpression(expressionStr);   // expressionStr可以是方法调用，如StringUtil.contains("yesss")
        // 使用表达式对象计算
        Object evaluate = expression.evaluate(context);
        // 输出结果：true
        System.out.println(evaluate);
    }

}
