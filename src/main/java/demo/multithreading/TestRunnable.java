package demo.multithreading;

// 实现runnable接口，重写run()方法，执行线程需要丢入runnable接口实现类，调用start方法
public class TestRunnable implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i < 200; ++i) {
            System.out.println("看代码" + i + "分钟");
        }
    }

    public static void main(String[] args) {

        // 创建Runnable接口的实现类对象
        TestRunnable testRunnable = new TestRunnable();
        // 创建线程对象，通过线程对象来开启我们的线程
        Thread thread = new Thread(testRunnable);
        // 调用start()方法开启线程
        thread.start();

        for (int i = 0; i < 200; ++i) {
            System.out.println("多线程" + i + "分钟");
        }
    }

}
