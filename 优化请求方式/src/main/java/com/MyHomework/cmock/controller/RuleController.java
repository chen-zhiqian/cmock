package com.MyHomework.cmock.controller;


import com.MyHomework.cmock.dao.MockInfo;
import com.MyHomework.cmock.service.BuildRuleService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;


@RestController
@CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping("/mock")
public class RuleController {

    @Autowired
    BuildRuleService buildRulesService;

    // @RequestBody直接将传入的json数据添加到对象中
    @RequestMapping(value = "/build", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public String buildRule(@RequestBody MockInfo mockInfo) throws IOException {

        // 获取请求中的json数据，将数据传入本地json文件中
        String response = buildRulesService.buildRule(mockInfo);

        return response;

    }

}
