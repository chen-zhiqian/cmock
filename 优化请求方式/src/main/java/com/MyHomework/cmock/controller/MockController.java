package com.MyHomework.cmock.controller;

import com.MyHomework.cmock.service.FixedRulePostService;
import com.MyHomework.cmock.service.DoGetService;
import com.MyHomework.cmock.service.DoPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;



@RestController
@CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping("/mock")
public class MockController {


    @Autowired
    private FixedRulePostService fixedRulePostService;

    @Autowired
    private DoPostService doPostService;

    @Autowired
    private DoGetService doGetService;


    @RequestMapping(value = "/p", method = RequestMethod.POST)
    public String mockPost(HttpServletRequest request) throws ExecutionException, InterruptedException {

        CompletableFuture<String> response = fixedRulePostService.DoRule(request.getParameterMap());

        return response.get();
    }

    // 通过配置的规则进行mock
    @RequestMapping(value = "/config/g", method = RequestMethod.GET)
    public String mockConfigGet(HttpServletRequest request) throws IOException, InterruptedException {

//    Map<String, String[]> requestMap = request.getParameterMap();
        HashMap requestMap = new HashMap(request.getParameterMap());

        String response = doGetService.DoGet(requestMap);

    return response;
    }

    // 通过配置的规则进行mock
    @RequestMapping(value = "/config/p", method = RequestMethod.POST)   // 还是接Map
    public String mockConfigPost(HttpServletRequest request) throws ExecutionException, InterruptedException, IOException {

        String response = doPostService.DoPost(request);

        return response;
    }

}

