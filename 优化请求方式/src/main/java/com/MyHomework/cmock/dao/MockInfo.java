package com.MyHomework.cmock.dao;

import java.util.List;

public class MockInfo {

    private String mockName;

    private int requestParamNum;

    private String requestParamTemplate;

    private List<MockInfo.MockConditionInfo> responseCondition;


    public String getMockName() {
        return mockName;
    }

    public void setMockName(String mockName) {
        this.mockName = mockName;
    }

    public int getRequestParamNum() {
        return requestParamNum;
    }

    public void setRequestParamNum(int requestParamNum) {
        this.requestParamNum = requestParamNum;
    }

    public String getRequestParamTemplate() {
        return requestParamTemplate;
    }

    public void setRequestParamTemplate(String requestParamTemplate) {
        this.requestParamTemplate = requestParamTemplate;
    }

    public List<MockInfo.MockConditionInfo> getResponseCondition() {
        return responseCondition;
    }

    public void setResponseCondition(List<MockInfo.MockConditionInfo> responseCondition) {
        this.responseCondition = responseCondition;
    }

    public static class MockConditionInfo {

        public String resCondition;

        public String resValue;

        public String getResCondition() {
            return resCondition;
        }

        public void setResCondition(String resCondition) {
            this.resCondition = resCondition;
        }

        public String getResValue() {
            return resValue;
        }

        public void setResValue(String resValue) {
            this.resValue = resValue;
        }

    }

}
