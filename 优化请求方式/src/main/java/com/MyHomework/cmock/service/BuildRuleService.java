package com.MyHomework.cmock.service;

import com.MyHomework.cmock.dao.MockInfo;

import java.io.IOException;

public interface BuildRuleService {

    String buildRule(MockInfo mockInfo) throws IOException;

}
