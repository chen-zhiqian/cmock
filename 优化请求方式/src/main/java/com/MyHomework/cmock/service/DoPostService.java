package com.MyHomework.cmock.service;

import com.MyHomework.cmock.dao.MockInfo;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface DoPostService {

    String DoPost(HttpServletRequest request) throws InterruptedException, IOException;

}
