package com.MyHomework.cmock.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public interface DoGetService {

    public String DoGet(HashMap<String, String[]> requestMap) throws IOException;

}
