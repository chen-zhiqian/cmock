package com.MyHomework.cmock.service.impl;

import com.MyHomework.cmock.dao.MockInfo;
import com.MyHomework.cmock.service.BuildRuleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
public class BuildRuleServiceImpl implements BuildRuleService {

    @Override
    public String buildRule(MockInfo mockInfo) throws IOException {

        boolean flag = checkRule(mockInfo);   // 先检查规则是否符合正确的格式

        if (flag) {

            checkSavePath();   // 检查文件夹
            makeJsonFile1(mockInfo.getMockName());    // 创建json文件
            writeJsonRule(mockInfo);       // 将数据写成Json文件

            return "规则写入成功^V^";
        }

        return "规则格式有误，请重新输入！";
    }

    // 向本地写入JSON数据
    private void writeJsonRule(MockInfo mockInfo) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File("C:\\mockRule\\" + mockInfo.getMockName() + ".json"), mockInfo);

    }

    // 判断Json文件是否合规
    private boolean checkRule(MockInfo mockInfo){


        // 这里的判断先默认正确，没想好要怎么判断
        // 目前做法是：在规则判断过程中对规则进行验证(try catch)


        return true;
    }

    // 创建Json文件
    private void makeJsonFile1(String name) throws IOException {

        File file = new File("C:\\mockRule\\" + name + ".json");
        if(!file.exists()) {
            file.createNewFile();//创建文件
            System.out.println("创建json文件" + name);
        } else {
            System.out.println("json文件" + name + "已存在，" + "新的内容已将其覆盖！");
        }

    }

    // 检查目录C:\mockRule是否存在
    private void checkSavePath() {

        File file = new File("C:\\mockRule");
        if(!file .exists()) {
            file.mkdirs();//创建目录
            System.out.println("创建目录mockRule，路径为：C:\\mockRule");
        } else {
            System.out.println("所有规则保存在C:\\mockRule目录下。");
        }

    }

}
