package demo.callback;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

public class SendPostRequest {

    // 利用RestTemplate发送post请求
    public static String postInvocation(String url, MultiValueMap<String, Object> param) {
        RestTemplate restTemplate = new RestTemplate();
        //设置Http Header
        HttpHeaders headers = new HttpHeaders();
        //设置请求媒体数据类型
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        //设置返回媒体数据类型
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(param, headers);
        return restTemplate.postForObject(url, httpEntity, String.class);
    }

    public static void main(String[] args) {

        MultiValueMap paramMap = new LinkedMultiValueMap();
//        paramMap.put("Response", "你能收到我的消息吗？？？");

        // 如果要加上回调功能，大概就在这个位置写入
        postInvocation("http://localhost:8888/callback/", paramMap);

    }

}
