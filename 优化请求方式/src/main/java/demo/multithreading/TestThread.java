package demo.multithreading;

// 继承Thread类，重写run()方法，调用start()开启线程
public class TestThread extends Thread{

    @Override
    public void run() {
        for (int i = 0; i < 200; ++i) {
            System.out.println("看代码" + i + "分钟");
        }
    }

    public static void main(String[] args) {

        // 创建线程对象
        TestThread testThread = new TestThread();
        // 调用start()方法开启线程
        testThread.start();

        // main线程
        for (int i = 0; i < 200; ++i) {
            System.out.println("多线程" + i + "分钟");
        }
        // 结果：主线程与子线程交替执行
    }
}
